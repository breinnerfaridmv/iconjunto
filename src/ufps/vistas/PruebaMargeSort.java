/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import java.util.ArrayList;
import java.util.List;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author Casa
 */
public class PruebaMargeSort {
    public static void main(String[] args) {
     ListaS<Integer> ejemploLista=new ListaS();
     int v [] = {1,2,3};    
        //Creando una lista de prueba:
        ejemploLista.insertarAlFinal(1);
        ejemploLista.insertarAlFinal(2);
        ejemploLista.insertarAlFinal(3);
        ejemploLista.insertarAlFinal(4);
        ejemploLista.insertarAlFinal(5);
        ejemploLista.insertarAlFinal(6);
        ejemploLista.insertarAlFinal(7);
        ejemploLista.insertarAlFinal(8);
        ejemploLista.insertarAlFinal(9);
        ejemploLista.insertarAlFinal(10);
        ejemploLista.insertarAlFinal(11);
        ejemploLista.insertarAlFinal(12);
        ejemploLista.insertarAlFinal(13);
        ejemploLista.insertarAlFinal(14);
//        ejemploLista.insertarAlFinal(15);
//        ejemploLista.insertarAlFinal(16);
//        List<List<Integer>> y = subsets(v);
        ListaS<ListaS<Integer>> x = new ListaS<>();
//        System.out.println(y.toString());
          x = subsets(ejemploLista);
//        
        System.out.println("\n\nMi conjunto par:"+getString_MultiLista(x));
    }
    
//    public static ListaS<ListaS<Integer>> subsets(ListaS<Integer> nums) {
//        ListaS<ListaS<Integer>> ans = new ListaS<>();
//        search(ans,new ListaS<>(),nums,0);
//        return ans;
//    }
//    
//    private static void search(ListaS<ListaS<Integer>> ans,ListaS<Integer> list,ListaS<Integer> nums,int start) {
//        if (start == nums.getTamanio()) {
//            ans.insertarAlInicio(new ListaS<>());
//            return;
//        }
//        
//        list.insertarAlInicio(nums.get(start));
//        search(ans,list,nums,start+1);
//        list.eliminar(list.getTamanio()-1);
//        search(ans,list,nums,start+1);
//    }
    
    public static ListaS<ListaS<Integer>> subsets(ListaS<Integer> nums) {
          ListaS<ListaS<Integer>> result = new ListaS<>();
        int n=nums.getTamanio();
        int size=(1<<n);
        for(int i=0;i<size;i++){ 
            int j=0;
            int num=i;
           ListaS<Integer> x=new ListaS<>();
            while(num>0){
                if((num&1)!=0){
                    x.insertarAlInicio(nums.get(j));
                }
                num>>=1;
                j++;
                
            }
         result.insertarAlInicio(x);
         
        }
        return result;
    }
    
    private static <T> String getString_MultiLista(ListaS<ListaS<T>> myLista)
    {
    
        if(myLista==null || myLista.esVacia())
            return "MultiLista Vacía";
        
        String msg="{ ";
        for(ListaS<T> myListaInterna: myLista)
        {
            msg+="(";
            for(T elemento:myListaInterna)
            {
            msg+=elemento.toString()+",";
            }
            //Eliminar la última ",";
            msg = msg.substring(0, msg.length() - 1);
            msg+=")\n";
        }
        //Eliminar el último "\t";
        msg = msg.substring(0, msg.length() - 1);
        msg = msg.substring(0, msg.length() - 1);
        msg+="( )}";
        return msg;
    } 
}
    
//    public static ListaS<ListaS<Integer>> sort(ListaS<Integer> lista, int izq, int der){
//      ListaS<ListaS<Integer>> conjuntos = new ListaS<>();
//      if(izq < der){
//      //Encuentra el punto medio del vector.
//      int mitad = (izq + der) / 2;  
//      //Divide la primera y segunda mitad (llamada recursiva).
//      sort(lista, izq, mitad);
//      sort(lista, mitad+1, der);
//
//      //Une las mitades.
//      merge(conjuntos,lista, izq, mitad, der);
//    }
//      return conjuntos;
//   }
//    
// public static void merge(ListaS<ListaS<Integer>> conjuntos,ListaS<Integer> lista, int izq, int mitad, int der) {
//  //Encuentra el tamaño de los sub-vectores para unirlos.
//  int n1 = mitad - izq + 1;
//  int n2 = der - mitad;
//
////  //Listas temporales.
//  ListaS<Integer> lista1 = new ListaS<>();
//  ListaS<Integer> lista2 = new ListaS<>();
////
////  //Copia los datos a las  temporales.
//   for (int i=0; i < n1; i++) {
//      
//         
//         
//         }
////  for (int j=0; j < n2; j++) {
////    derArray[j] = lista[mitad + j + 1];
////  }
////  /* Une los vectorestemporales. */
////
////  //Índices inicial del primer y segundo sub-vector.
////  int i = 0, j = 0;
////
////  //Índice inicial del sub-vector arr[].
////  int k = izq;
////
////  //Ordenamiento.
////  while (i < n1 && j < n2) {
////    if (izqArray[i] <= derArray[j]) {
////      arr[k] = izqArray[i];
////      i++;
////    } else {
////        arr[k] = derArray[j];
////        j++;
////    }
////    k++;
////  }//Fin del while.
////
////  /* Si quedan elementos por ordenar */
////  //Copiar los elementos restantes de izqArray[].
////  while (i < n1) {
////    arr[k] = izqArray[i];
////    i++;
////    k++;
////  }
////  //Copiar los elementos restantes de derArray[].
////  while (j < n2) {
////    arr[k] = derArray[j];
////    j++;
////    k++;
////  }
//}

