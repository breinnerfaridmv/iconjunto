/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author MADARME
 */
public class Test_ConjuntoPar {
    
    public static void main(String[] args) {     
        ListaS<Integer> ejemploLista=new ListaS();
        
        //Creando una lista de prueba:
        ejemploLista.insertarAlFinal(1);
        ejemploLista.insertarAlFinal(2);
        ejemploLista.insertarAlFinal(3);
        ejemploLista.insertarAlFinal(4);
        ejemploLista.insertarAlFinal(5);
        ejemploLista.insertarAlFinal(6);
        ejemploLista.insertarAlFinal(7);
        ejemploLista.insertarAlFinal(8);
        ejemploLista.insertarAlFinal(9);
//        ejemploLista.insertarAlFinal(10);
//        ejemploLista.insertarAlFinal(11);
//        ejemploLista.insertarAlFinal(12);
//        ejemploLista.insertarAlFinal(13);
//        ejemploLista.insertarAlFinal(14);
//        ejemploLista.insertarAlFinal(15);
//        ejemploLista.insertarAlFinal(16);
        
        //Llamando el método conjuntoPar:
        ListaS<ListaS<Integer>> par=ejemploLista.getConjuntoPar();
        //Imprimir caso de prueba:
        System.out.println("\n\nMi conjunto par:"+getString_MultiLista(par));
        
    }
    
    private static <T> String getString_MultiLista(ListaS<ListaS<T>> myLista)
    {
    
        if(myLista==null || myLista.esVacia())
            return "MultiLista Vacía";
        
        String msg="{ ";
        for(ListaS<T> myListaInterna: myLista)
        {
            msg+="(";
            for(T elemento:myListaInterna)
            {
            msg+=elemento.toString()+",";
            }
            //Eliminar la última ",";
            msg = msg.substring(0, msg.length() - 1);
            msg+=")\n";
        }
        //Eliminar el último "\t";
        msg = msg.substring(0, msg.length() - 1);
        msg = msg.substring(0, msg.length() - 1);
        msg+="( )}";
        return msg;
    } 
}
